(defproject mimosa "0.1.0"
  :description "A Music Player for beets"
  :url ""
  :license {:name "MIT"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.9.946"]
                 [org.clojure/core.async "0.4.474"]
                 [cljs-http "0.1.44"]

                 [re-frame "0.10.5"]
                 [reagent "0.8.0-alpha2"]
                 [secretary "1.2.3"]

                 [org.roman01la/cljss "1.6.2"]

                 [com.cemerick/piggieback "0.2.1"]
                 [figwheel-sidecar "0.5.13"]]

  :plugins [[lein-cljsbuild "1.1.7"]
            [lein-shell "0.5.0"]
            [lein-npm "0.6.2"]
            [lein-figwheel "0.5.13"]
            [lein-modules "0.3.11"]]



  :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}

  :cljsbuild {:builds {:main {:source-paths ["src/app"]
                              :incremental true
                              :assert true
                              :compiler {:output-to ".out/app/app.js"
                                         :output-dir ".tmp/app"
                                         :warnings true
                                         :elide-asserts true
                                         :target :nodejs
                                         :optimizations :simple
                                         :pretty-print true
                                         :output-wrapper true}}
                       :ui {:source-paths ["src/ui"]
                            :incremental true
                            :assert true
                            :figwheel {:websocket-host :server-ip
                                       :on-jsload mimosa.ui.core/on-reload}

                            :compiler {:output-to ".out/app/ui.js"
                                       :output-dir ".out/app/lib-ui"
                                       :warnings true
                                       :elide-asserts true
                                       :optimizations :none
                                       :pretty-print true
                                       :output-wrapper true}}}}

  :aliases {"app" ["do"
                   ["shell" "cp" "electron.package.json" ".out/app/package.json"]
                   ["cljsbuild" "once" "main"]]
            "ui" ["do"
                  ["shell" "cp" "resources/index.html" ".out/app/"]
                  ["shell" "cp" "resources/fontawesome.css" ".out/app/css/"]
                  ["shell" "cp" "-r" "resources/webfonts" ".out/app/"]
                  ["cljsbuild" "once" "ui"]]
            "electron" ["do"
                        ["shell" "cp" "electron.package.json" ".out/app/package.json"]
                        ["shell" "cp" "resources/index.html" ".out/app/"]
                        ["shell" "cp" "-r" "resources/webfonts" ".out/app/"]
                        ["shell" "cp" "resources/fontawesome.css" ".out/app/css/"]
                        ["cljsbuild" "auto" "main"]]}
  :figwheel {:nrepl-port 7888
             :nrepl-middleware ["cemerick.piggieback/wrap-cljs-repl"]
             :server-logfile ".out/app/figwheel-logfile.log"
             :css-dirs ["resources/public/css"]
             :auto-clean false})
