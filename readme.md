# Mimosa

## Develop

```
mkdir -p .out/app

# wait for compile and dev server
./dev.sh

# client, absolute path might be required
electron [path-to-.out/app]
```

## Release

```
lein electron once
cd .out/app
yarn dist
```
