#!/bin/bash

set -e

echo "lein deps..."

# actual deps
lein deps


# lein electron: electron server, auto reloads src/app, alias for `lein cljsbuild auto main`
# lein figwheel: # frontend dev server, auto reloads src/ui

tmux \
	new-session 'lein electron' \; \
	split-window 'lein figwheel' \; \
	split-window 'beet web' \; \
	select-layout tiled \; \
	# detach-client
