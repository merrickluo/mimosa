(ns mimosa.ui.view.app
  (:require-macros [secretary.core :refer [defroute]]
                   [cljss.reagent :refer [defstyled]])

  (:require [re-frame.core :refer [dispatch subscribe]]
            [reagent.core :as reagent :refer [atom]]
            [secretary.core :as secretary]
            [cljss.reagent]

            [mimosa.ui.view.player :refer [player]]
            [mimosa.ui.view.albums-list :refer [album-list]]
            [mimosa.ui.view.track-list :refer [track-list]]
            [mimosa.ui.view.settings :refer [settings]]))


(defroute "/albums" []
  (dispatch [:select-page :albums]))
(defroute "/tracks" []
  (dispatch [:select-page :library]))

(defmulti page-content #(-> @(subscribe [:current-page])))
(defmethod page-content :albums []
  [album-list])
(defmethod page-content :library []
  [track-list])

(defstyled container :div
  {:display "flex"
   :flex-direction "column"
   :widht "100%"
   :height "100%"})

(defstyled header :header
  {:flex "0 0 auto"})

(defstyled content :main
  {:flex "1 1 auto"
   :position "relative"})

(defstyled page-wrapper :div
  {:position "absolute"
   :width "100%"
   :height "100%"
   :max-height "100%"})

(defstyled footer :footer
  {:flex "0 0 auto"
   :background-color "#f4f4f4"
   :z-index "99"
   :width "100%"})

(defn app []
  [container
   [header
    [player]]
   ;; dynamic style cannot be in defstyled? (var?)
   [content
    [page-wrapper
     [page-content]]]
   [footer
    [:button {:on-click #(secretary/dispatch! "albums")} "Albums"]
    [:button {:on-click #(secretary/dispatch! "tracks")} "Tracks"]]])
