(ns mimosa.ui.view.track-list
  (:require-macros [cljss.reagent :refer [defstyled]])
  (:require [re-frame.core :refer [subscribe dispatch]]
            [reagent.core :as r]
            [cljss.reagent]
            [goog.string :as string]
            [goog.string.format]

            [mimosa.ui.styles :as s]))

(defonce local-state (r/atom {:selected-track-id nil}))

(def track-height 30)

;; components
(defstyled header-text :div
  {:border-left "1px solid #d2d2d2"
   :border-left-margin-top "4px"
   :display "flex"
   :padding-left "4px"
   :white-space "nowrap"
   :align-items "center"})

(defstyled item-text :div
  {:padding-left "4px"
   :text-overflow "ellipsis"
   :text-size-adjust "100%"
   :line-height "24px"
   :white-space "nowrap"
   :overflow "hidden"})

(defn header-cell [title width]
  [:div {:class [(s/cell width) (s/track-header-cell)]}
   [header-text title]])

(defn track-header []
  [:div {:class (s/track-header)}
   [:div {:class (s/track-row)}
    [:div {:class (s/cell "18px")}]
    [header-cell "#" "24px"]
    [header-cell "Title" "30%"]
    [header-cell "Duration" "60px"]
    [header-cell "Artist" "15%"]
    [header-cell "Album" "30%"]
    [header-cell "Genre" ""]]])

(defn track-cell [text width]
  [:div {:class (s/cell width)}
   [item-text text]])

(defn track-item [track selected tracks]
  [:div {:class [(s/track-row)
                 (if selected (s/track-row-selected))
                 "track-row-extra"]
         :on-double-click #(dispatch [:select-track track tracks])
         :on-mouse-down #(swap! local-state assoc :selected-track-id (:id track))}
   [:div {:class (s/cell "18px")}] ;; TODO play state
   [track-cell (:track track) "24px"]
   [track-cell (:title track) "30%"]
   [track-cell (:duration track) "60px"]
   [track-cell (:artist track) "15%"]
   [track-cell (:album track) "30%"]
   [track-cell (:genre track) ""]])

(defonce skip-tile-count (r/atom 0))
(def track-tile-size 20)
(def track-tile-count 4)

(defn track-tile [idx chunk tracks]
  (let [distance (* idx 30 20)]
    [:div {:style {:top distance
                   :position "absolute"
                   :width "100%"}}
     (for [track chunk]
       (let [selected (= (:selected-track-id @local-state) (:id track))]
         ^{:key (:id track)} [track-item track selected tracks]))]))

(defn track-tiles [tracks]
  (let [height (* track-tile-count (count tracks))
        skiped @skip-tile-count
        chunks (partition track-tile-size tracks)]
    [:div {:style {:position "absolute"
                   :width "100%"}}
     (if-not (empty? chunks)
       (for [idx (range skiped (+ skiped track-tile-count))]
         (if-let [chunk (first (drop idx chunks))]
           ^{:key idx} [track-tile idx chunk tracks])))]))


(defn on-scroll [event]
  (let [top (.-scrollTop (.-target event))
        tile-height (* track-tile-size 30)
        skip-count (Math/floor (/ top tile-height))]
    (reset! skip-tile-count skip-count)))

(defn track-list []
  (let [keyword @(subscribe [:search-keyword])
        tracks @(subscribe [:tracks keyword])]
    [:div {:class (s/content)}
     [track-header]
     [:div {:class (s/track-list)
            :id "track-list"
            :on-scroll on-scroll}
      [track-tiles tracks]]]))
