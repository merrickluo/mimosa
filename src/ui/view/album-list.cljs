(ns mimosa.ui.view.albums-list)

(defn album-item [album]
  [:li
   [:p (:album album)]])

(defn album-list [albums]
  [:div
   [:p "Albums"]])
