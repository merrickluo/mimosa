;; from https://gist.github.com/Deraen/8b631c587335a71f9862b744c65ac37b
(ns mimosa.ui.view.fontawesome
  (:require [cljsjs.react]))

(defn icon [{:keys [name class size rotate flip fixed-width spin pulse stack inverse component]
             :or {component :span}
             :as props}]
  [component
   (assoc props
          :class (str "fa fa-" class " "
                      (if size (str "fa-" size)) " "
                      (if rotate (str "fa-rotate-" rotate)) " "
                      (if flip (str "fa-flip-" flip)) " "
                      (if fixed-width "fa-fw") " "
                      (if spin "fa-spin") " "
                      (if pulse "fa-pulse") " "
                      (if stack (str "fa-stack-" stack)) " "
                      (if inverse "fa-inverse")))])
