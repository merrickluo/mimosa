(ns mimosa.ui.view.player
  (:require-macros [cljss.reagent :refer [defstyled]])
  (:require [re-frame.core :refer [subscribe dispatch]]
            [reagent.core :as r]
            [cljss.reagent]

            [mimosa.ui.view.fontawesome :refer [icon]]))

(defn get-audio-player []
  (.getElementById js/document "player"))

;; mannualy set volume cause react not support volume tag
(defn update-audio [state]
  (let [audio (get-audio-player)
        track (:track @state)
        volume (:volume @state)
        playing (:playing @state)]
    (when-not (or (nil? audio) (nil? track))
      (set! (.-volume audio) (/ volume 100))
      (if-not (= (.-src audio) (:url track))
        (set! (.-src audio) (:url track)))
      (if playing
        (.play audio)
        (.pause audio)))))

(defn search
  [event]
  (let [keyword (.-value (.-target event))]
    (dispatch [:update-filter keyword])))

(defstyled layout-container :div
  {:display "flex"
   :width "100%"
   :justify-content "space-between"
   :background-color "#f4f4f4"
   :height "48px"})

(defstyled layout-controls :div
  {:display "flex"
   :align-items "center"
   :justify-content "space-between"
   :padding-left "16px"
   :width "100px"
   :height "48px"})

(defstyled layout-info :div
  {:display "flex"
   :align-items "center"
   :justify-content "center"})

(defstyled layout-search :div
  {:display "flex"
   :align-items "center"
   :margin-right "16px"})

(defstyled search-container :div
  {:border "1px solid #d2d2d2"
   :border-radius "4px"
   :background-color "#fff"
   :padding "4px"})

(defstyled search-input :input
  {:border "none"
   :outline "none"
   :margin-left "4px"})

(defn search-box [props]
  [search-container
   [icon {:class "search"}]
   [search-input props]])

(defn player-display []
  "render player contorls"
  (let [state (subscribe [:playing])
        show-volume (r/atom false)]
    (fn []
      (update-audio state)
      [layout-container
       (if @show-volume
         ;; volume slider
         [layout-controls {:on-mouse-enter #(reset! show-volume true)
                           :on-mouse-leave #(reset! show-volume false)}
          ;; [ant/slider {:style {:width 80}
          ;;              :default-value volume
          ;;              :max 100
          ;;              :min 0
          ;;              :on-change #(dispatch [:change-volume %])}]
          ;; [ant/button {:ghost true
          ;;              :size "small"
          ;;              :shape "circle"
          ;;              :icon "sound"}]
          ]

         ;; player controls
         [layout-controls
          [icon {:class "backward"
                 :on-click #(dispatch [:prev])}]
          [icon {:class (if (:playing @state) "pause" "play")
                 :size "2x"
                 :on-click #(dispatch [:play-pause])}]
          [icon {:class "forward"
                 :on-click #(dispatch [:next])}]
          [icon {:class "volume-up"
                 :on-mouse-enter #(reset! show-volume true)}]])

       ;; track info
       [layout-info
        [:p (or (:title (:track @state)) "Not Playing")]]

       [layout-search
        [search-box {:placeholder "search"
                     :on-change #(search %)}]]])))

;; wrap audio
(defn player []
  [layout-container
   [:audio {:id "player"
            :onEnded #(dispatch [:next])}]
   [player-display]])
