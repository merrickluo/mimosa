;; global style inject
(ns mimosa.ui.styles
  (:require-macros [cljss.core :refer [defstyles inject-global font-face]])
  (:require [cljss.core]))

;; FIXME cljss should convert numberic value to px
;; https://github.com/roman01la/cljss/issues/33
(defn inject-styles []
  (inject-global
   ;; hide scrollbar
   {"::-webkit-scrollbar" {:height "0px"
                           :width "0px"
                           :background "transparent"}
    :html {:height "100%"
           :font-family "Source Sans Pro, sans-serif"
           :width "100%"}
    :body {:font-size "0.8em"
           :margin "0px"
           :overflow "hidden"
           :height "100%"
           :color "#333"}
    "div#app-container" {:width "100%"
                         :height "100%"}
    ;; child selector not supported
    ".track-row-extra:nth-child(odd)" {:background-color "#f4f4f4"}})

  (font-face
   {:font-family "Source Sans Pro, sans-serif"}))

(def track-height 30)

(defstyles content []
  {:width "100%"
   :height "100%"})

(defstyles track-header []
  {:width "100%"
   :height "30px"
   :z-index "99"
   :background-color "#ffffff"
   :border-top "1px solid #d2d2d2"
   :border-bottom "1px solid #d2d2d2"})

(defstyles track-list []
  {:height "100%"
   :position "absolute"
   :top "30px"
   :left "0px"
   :right "0px"
   :bottom "0px"
   :overflow "auto"})

(defstyles track-row []
  {:width "100%"
   :display "flex"
   :height (str track-height "px")
   :flex-direction "row"})

(defstyles track-row-selected []
  {:color "white"
   :background-color "#459CE7 !important"})

(defstyles cell [width]
  {:width width
   :overflow "hidden"
   :text-overflow "ellipsis"
   :user-select "none"
   :vertical-align "middle"
   :cursor "default"
   :margin-top "4px"
   :margin-bottom "4px"
   :line-height "30px"})

(defstyles track-header-cell []
  {:font-weight "bold"
   :display "flex"
   :align-items "center"
   :flex-direction "row"})
