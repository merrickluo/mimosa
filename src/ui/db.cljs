(ns mimosa.ui.db
  (:require [re-frame.core :as rf]
            [cljs.reader :as reader]))

(def default-app-db
  {:data {:albums []
          :tracks []}
   :playing-state {:track nil
                   :playing false
                   :queue []
                   :volume 50}
   :display-state {:height 600
                   :page :library}
   :settings {:beets {:server "http://dayakkaiser.luois.ninja:8337"}}})

(def ls-db-key "saved-db")

(defn db->local-store
  "saving current db into local storage"
  [db]
  (.setItem js/localStorage ls-db-key (str db)))

(rf/reg-cofx
 :cached-db
 (fn [cofx _]
   (assoc cofx :cached-db
          (into {}
                (some->> (.getItem js/localStorage ls-db-key)
                         (reader/read-string))))))
