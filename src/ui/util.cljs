(ns mimosa.ui.util
  (:require [cljs.pprint :refer [pprint]]))

(defn log [& args]
  (apply (.-log js/console) (clj->js args)))
