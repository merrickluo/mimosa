(ns mimosa.ui.subs
  (:require [re-frame.core :as rf]
            [clojure.string :as string]))


(defn filtered-tracks [tracks keyword]
  (if (string/blank? keyword)
    tracks
    (filter (fn [track]
              ;; field might be nil
              (let [fields [(or (:title track) "")
                            (or (:album track) "")
                            (or (:artist track) "")]
                    lcase-keyword (string/lower-case keyword)
                    lcase-fields (map string/lower-case fields)]
                (some #(string/includes? % lcase-keyword)
                      lcase-fields)))
            tracks)))


(rf/reg-sub
 :tracks
 (fn [db [_ keyword]]
   (let [tracks (filtered-tracks (-> db :data :tracks) keyword)]
     (sort-by (juxt :album :track) tracks))))

(rf/reg-sub
 :current-page
 (fn [db]
   (-> db :display-state :page)))

(rf/reg-sub
 :content-height
 (fn [db _]
   (-> db :display-state :height)))

(rf/reg-sub
 :search-keyword
 (fn [db _]
   (-> db :display-state :search-keyword)))

(rf/reg-sub
 :playing

 (fn [db]
   (:playing-state db)))
