(ns mimosa.ui.events
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs.core.async :refer [<!]]
            [re-frame.core :as rf]
            [clojure.string :as string]

            [mimosa.ui.util :refer [log]]
            [mimosa.ui.db :refer [default-app-db db->local-store]]
            [mimosa.ui.provider.beets :as beets]))

(def ->local-store (rf/after db->local-store))

(rf/reg-event-fx
 :initialize-db

 ;; run :cached-db cofx defined in db.cljs
 [(rf/inject-cofx :cached-db)]

 (fn [{:keys [_ cached-db]} _]
   {:db (merge default-app-db cached-db)}))

(rf/reg-event-db
 :loaded-from-server
 ;; [->local-store]

 (fn [db [_ data]]
   (-> db
       (assoc-in [:data :albums] (:albums data))
       (assoc-in [:data :tracks] (:tracks data)))))

(rf/reg-event-db
 :reload-library

 (fn [db]
   (go (let [server (-> db :settings :beets :server)
             albums (<! (beets/get-albums server))
             tracks (<! (beets/get-tracks server))]
         (log "loaded" (count albums) "albums from server")
         (log "loaded" (count tracks) "tracks from server")
         (log (first tracks))
         (rf/dispatch [:loaded-from-server {:albums albums
                                            :tracks tracks}])))
   db))

(rf/reg-event-db
 :resize-window

 (fn [db [_ new-height]]
   (assoc-in db [:display-state :height] new-height)))

(rf/reg-event-db
 :select-page
 ;; [->local-store]

 (fn [db [_ page]]
   (assoc-in db [:display-state :page] page)))


(rf/reg-event-db
 :select-track

 ;; [->local-store]

 (fn [db [_ track queue]]
   (-> db
       (assoc-in [:playing-state :track] track)
       (assoc-in [:playing-state :queue] queue)
       (assoc-in [:playing-state :playing] true))))

(rf/reg-event-db
 :change-volume

 (fn [db [_ volume]]
   (assoc-in db [:playing-state :volume] volume)))

(rf/reg-event-db
 :update-filter

 (fn [db [_ keyword]]
   (assoc-in db [:display-state :search-keyword] keyword)))

(rf/reg-event-db
 :play-pause

 (fn [db]
   (assoc-in db [:playing-state :playing]
             (not (-> db :playing-state :playing)))))

(rf/reg-event-db
 :prev

 (fn [db]
   (let [current (-> db :playing-state :track)
         queue (-> db :playing-state :queue)
         idx (.indexOf queue current)]
     (if-not (or (nil? idx) (= idx 0))
       (assoc-in db [:playing-state :track] (nth queue (dec idx)))
       db))))

(rf/reg-event-db
 :next

 (fn [db]
   (let [current (-> db :playing-state :track)
         queue (-> db :playing-state :queue)
         idx (.indexOf queue current)]

     (if-not (or (nil? idx) (= idx (count queue)))
       (assoc-in db [:playing-state :track] (nth queue (inc idx)))
       db))))
