(ns mimosa.ui.core
  (:require-macros [reagent.core :refer [with-let]])
  (:require [mimosa.ui.events]
            [mimosa.ui.subs]

            [reagent.core :as reagent]
            [re-frame.core :refer [dispatch-sync dispatch]]
            [cljss.core :as css]

            [mimosa.ui.util :refer [log]]
            [mimosa.ui.styles :refer [inject-styles]]
            [mimosa.ui.view.app :refer [app]]))

(enable-console-print!)

(defn update-content-size []
  (dispatch [:resize-window (.-innerHeight js/window)]))

(dispatch-sync [:initialize-db])
(dispatch-sync [:resize-window (.-innerHeight js/window)])

;; FIXME reload by a button
(dispatch [:reload-library])

(defn root-component []
  (with-let [_ (.addEventListener js/window "resize" update-content-size)]
    [app]
    (finally
      (log "remove event listener")
      (.removeEventListener js/window "resize" update-content-size))) )

(defn render-app []
  (inject-styles)
  (reagent/render
   [root-component]

   (js/document.getElementById "app-container")))

(defn on-reload []
  (log "reloading ui")
  (css/remove-styles!)
  (render-app))

(render-app)
