(ns mimosa.ui.provider.beets
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs-http.client :as http]
            [cljs.core.async :refer [<!]]
            [goog.string :as string]
            [goog.string.format]

            [mimosa.ui.util :refer [log]]))

(defn ^:private fetch [server path first-key]
  (let [url (str server "/" path "/")]
    (log url)
    (go (let [resp (<! (http/get url))]
          (if (:success resp)
            (-> resp :body first-key)
            {:error (str "error fetching " path " from" url)})))))

(defn get-albums
  [server]
  (fetch server "album" :albums))

(defn ^:private stream-url-for-track
  [server track]
  (str server "/item/" (:id track) "/file"))

(defn ^:private duration-for-track
  [track]
  (let [len (:length track)
        minutes (int (/ len 60))
        seconds (int (mod len 60))]
    (string/format "%02d:%02d" minutes seconds)))

(defn get-tracks
  [server]
  (go (let [tracks (<! (fetch server "item" :items))]
        (map #(-> %
                  (assoc :url (stream-url-for-track server %))
                  (assoc :duration (duration-for-track %))) tracks))))
